
import static java.util.Collections.sort;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.io.File;
import java.io.FileNotFoundException;

public class SimulationManager  implements  Runnable{

    private int N;
    private int Q;
    private int tMaxSim;
    private int tMin_arrival;
    private int tMax_arrival;
    private int tMin_service;
    private int tMax_service;
    private String fileIn;
    private String fileOut;
    private  Scheduler scheduler;
    private List<Client> generatedClients;

    public SimulationManager(String fileIn, String fileOut)
    {
        this.fileIn=fileIn;
        this.fileOut=fileOut;
        readfile();
        scheduler=new Scheduler(Q);
        generateRandomClients();
    }

    private void readfile()
    {
        try {
            File myFile= new File(fileIn);
            Scanner myReader = new Scanner(myFile);
            myReader.useDelimiter("[\\s, \r\n]+");
            N= myReader.nextInt();
            Q= myReader.nextInt();
            tMaxSim=myReader.nextInt();
            tMin_arrival= myReader.nextInt();
            tMax_arrival= myReader.nextInt();
            tMin_service= myReader.nextInt();
            tMax_service= myReader.nextInt();
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    private void generateRandomClients() {
        generatedClients=new ArrayList<Client>();
        Random rand= new Random();
        Client client;

        for(int i=1;i<=N;i++) {
            client = new Client(i, 0, 0);
            client.setT_arrival(rand.nextInt((tMax_arrival - tMin_arrival) + 1) + tMin_arrival);
            client.setT_service(rand.nextInt((tMax_service - tMin_service) + 1) + tMin_service);

            generatedClients.add(client);
        }
        sort(generatedClients);
    }

    public String toString(int tSim, List<Client> generatedClients)
    {
        String t="";
        t+="\r\nTime " + tSim +"\r\n";
        t+="Waiting clients: ";
        for(Client c1: generatedClients)
            t+=c1.toString();
        t+="\r\n";
        t+=scheduler.toString();
        return t;
    }

    public void run() {
        try {
            FileWriter myWriter = new FileWriter(fileOut);
            int tSim=0; Client c=new Client(0,0,0);
            while(tSim<tMaxSim) {
                if(!generatedClients.isEmpty()) {
                    c = generatedClients.get(0);
                    while (c.getT_arrival() == tSim && !generatedClients.isEmpty()) {
                        scheduler.dispatch(c);
                        generatedClients.remove(c);
                        if (!generatedClients.isEmpty())
                            c = generatedClients.get(0);
                    }
                }
                myWriter.write(toString(tSim, generatedClients));
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    System.out.println("Thread was interrupted\n"); }
                tSim++;
                if(generatedClients.isEmpty() && scheduler.emptyQueue())
                    break;
            }
            myWriter.write("Average waiting time: "+Float.toString(Server.averageTime()));
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error ocured to writing to the file"); }
    }

    public static void main(String[] args){

        SimulationManager sm= new SimulationManager(args[0], args[1]);
        //SimulationManager sm= new SimulationManager("in-test-2.txt", "out-test-2.txt");
        Thread t=new Thread(sm);
        t.start();


    }
}
