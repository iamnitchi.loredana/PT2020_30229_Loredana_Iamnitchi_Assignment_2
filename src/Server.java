import java.util.Iterator;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable {

    private BlockingQueue<Client> clients;
    private AtomicInteger waitingTime;
    private static int totalTime;
    private static int nrOfClients;
    private volatile boolean flag;

    public Server() {
        this.clients = new ArrayBlockingQueue<Client>(500);
        this.waitingTime = new AtomicInteger(0);
        this.flag=false;
    }

    public void addClient(Client newClient) {
        clients.add(newClient);
        flag=true;
        waitingTime.set(waitingTime.get()+newClient.getT_service());
    }

    public void removeClient(Client c) {
        clients.remove(c);
        nrOfClients++;
        totalTime+=c.getT_wait();
        if(clients.isEmpty())
            flag=false;
    }

    public void run() {
        while (flag) {
            Client c=new Client(0,0,0);
            Iterator itr = clients.iterator();
            c=(Client)itr.next();
            if(c.getT_service()==0)
                removeClient(c);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            c.setT_service(c.getT_service() - 1);
            if(c.getT_service()==0)
                removeClient(c);
            waitingTime.decrementAndGet();
        }
    }

    public static float averageTime()
    {
        return (float)totalTime/nrOfClients;
    }

    public boolean empty(){
        if(clients.isEmpty())
            return true;
        else
            return false;
    }

    public int getWaitingTime() {
        return waitingTime.get();
    }

    public String toString() {
        String t="";
        for(Client c: clients)
            t+=c.toString();
        t+="\r\n";
        return t;
    }

}
