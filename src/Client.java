public class Client implements Comparable {

    private int id;
    private int t_arrival;
    private int t_service;
    private int t_wait;

    public Client(int id, int t_arrival, int t_service)
    {
        this.id=id;
        this.t_arrival=t_arrival;
        this.t_service=t_service;
        this.t_wait=0;
    }

    public int getT_arrival() {
        return t_arrival;
    }

    public void setT_arrival(int t_arrival) {
        this.t_arrival = t_arrival;
    }

    public int getT_service() {
        return t_service;
    }

    public void setT_service(int t_service) {
        this.t_service = t_service;
    }

    public int getT_wait() {
        return t_wait;
    }

    public void setT_wait(int t_wait) {
        this.t_wait = t_wait;
    }

    public int compareTo(Object o) {

        if(this.t_arrival <((Client)o).t_arrival) {
            return -1;
        }else if(this.t_arrival == ((Client)o).t_arrival) {
            return 0;
        }else {
            return 1;
        }
    }

    public String toString() {
        return "(" + id + "," + t_arrival + "," + t_service + "); ";
    }
}
