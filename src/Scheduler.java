import java.util.ArrayList;
import java.util.List;

public class Scheduler {

    private List<Server> servers=new ArrayList<Server>();


    public Scheduler(int maxNoServers)
    {
        for(int i=0; i< maxNoServers;i++) {
            Server s=new Server();
            servers.add(s);
        }
    }

    public void dispatch(Client c)
    {
        //aici implementez strategia de lucru, cum pun clientii in cozi
        int ok=0,min=9999;
        for(Server s: servers)
            if (s.empty() && ok == 0) {
                Thread thread=new Thread(s);
                thread.start();
                s.addClient(c);
                c.setT_wait(c.getT_service());
                ok = 1;
            }

        for(Server s: servers)
            if (!s.empty() && s.getWaitingTime() < min)
                min = s.getWaitingTime();

        for(Server s: servers)
            if (!s.empty() && s.getWaitingTime() == min && ok==0) {
                s.addClient(c);
                c.setT_wait(min+c.getT_service());
                ok=1;
            }
    }

    public boolean emptyQueue()
    {
        for(Server s: servers)
            if (!s.empty())
                return false;
         return true;
    }

    public String toString() {
        String t=""; int i=1;
        for(Server s: servers)
        {
            t+="Queue "+i+": ";
            i++;
            t+=s.toString();
        }
        t+='\n';
        return t;
    }
}
